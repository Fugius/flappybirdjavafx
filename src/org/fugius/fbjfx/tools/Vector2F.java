package org.fugius.fbjfx.tools;

public class Vector2F {
	public float x;
	public float y;
	
	public Vector2F() {
		this.x = 0;
		this.y = 0;
	}
	
	public Vector2F(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public Vector2F(Vector2F b) {
		x = b.x;
		y = b.y;
	}
	
	public Vector2F add(Vector2F b) {
		x += b.x;
		y += b.y;
		
		return this;
	}
	
	public Vector2F add(float a, int b) {
		x += a;
		y += b;
		
		return this;
	}
	
	public Vector2F multiply(float b) {
		x *= b;
		y *= b;
		
		return this;
	}
}
