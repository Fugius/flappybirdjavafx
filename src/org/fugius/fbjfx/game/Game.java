package org.fugius.fbjfx.game;

/*
 * Flappy bird mais avec des boutons
 * Java FX c'est assez degueu pour faire des jeux ou pour vouloir personnaliser des trucs
 * De ce fait, tout est degueu ici (le manque de motivation du a la lib j'imagine), tout dans un fichier full inner class 
 * Meme le score est compte de maniere degueu avec ce oiseau.x == tuyeau.x mega dangereux (mais qui marche a priori malgres la vitesse de defilement > 1 )
 * Si un jour j'ai le courrage, il faudra que je separe toutes ces classes
 * Seul effort : Update toute la logique 90 fois par secondes dans un thread separe
 * 
 * Realise par moi : Fugius
 *
 * Edit : Bonne chance à moi si je veu un jour éditer ce truc, c'est pas trop mal structuré mais rien n'est commenté et j'ai du faire pleins de trucs bizzares a cause de
 * la rigidité de java fx
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.fugius.fbjfx.tools.Vector2F;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import javafx.scene.*;

public class Game extends Application {
	
	private static Game instance;
	
	private Group root = null;
	private boolean lost = false;
	
	private int GateNumber;
	
	private Bird bird;
	private ArrayList<Pipe> pipes;
	private Button background;
		
	private Vector2F size;
	
	private boolean running = true;
	
	private int score = 0;
	private int maxScore = 0;
	private int lastScorePos = 0;
	
	private Button bScore;
	private Button bMaxScore;
	
	public static Game getInstance() {
		return instance;
	}
	
	public Game() {
		instance = this;
	}
	
	@Override
	public void init() {
		size = new Vector2F(1280, 720);
		GateNumber = 4;
		
		pipes = new ArrayList<>();
		
	}

	public void update() {
		if (root == null || lost)
			return;
				
		for (Pipe p : pipes) {
			p.update();
		}
		
		bird.update();
		
		for (Pipe p : pipes) {
			if (checkCollision(bird, p)) {
				lost = true;
				
				score = 0;
				
				Platform.runLater(new Runnable() {
					
					@Override
					public void run() {
						bScore.setText("Score : " + score);
					}
				});	
			}
		}
		
		if (!pipes.isEmpty()) {
			boolean scoreIncreased = false;
			for (Pipe p : pipes) {
				double d =  Math.abs(p.getPos().x - bird.getLayoutX());
				
				if (d <= 0) {
					scoreIncreased = true;
				}
			}
			
			if (scoreIncreased) {
				scoreIncreased = false;
				score++;
			}
			
			if (maxScore < score)
				maxScore = score;
			
			Platform.runLater(new Runnable() {
				
				@Override
				public void run() {
					bScore.setText("Score : " + score);
					bMaxScore.setText("Best Score : " + maxScore);
				}
			});	
		}
			 

	}
	

	@Override
	public void start(Stage stage) throws Exception {
		stage.setTitle("Flappy BUTTON ?");
		
		background = new Button();
		background.setPrefSize(size.x, size.y);
		background.setStyle(
                "-fx-base: #5dc6ef; " +
                "-fx-font-size: 35px; " +
                "-fx-text-background-color: whitesmoke;"
        );
		background.setText("SKY");
		
		bird = new Bird();
						
		root = new Group();
		root.getChildren().add(background);
		root.getChildren().add(bird);
				
		stage.setScene(new Scene(root, size.x, size.y));
		stage.show();
		
		root.setOnKeyPressed(e -> {
		    if (e.getCode() == KeyCode.D) {
		        bird.fly();
		    }
		});
		
		bird.setOnMouseClicked(e -> {
	
			for (int i = 0; i < GateNumber * 2; i += 2) {
				initPipe(pipes.get(i), i/2 * (size.x / GateNumber) + size.x);
				initPipe(pipes.get(i + 1), i/2 * (size.x / GateNumber) + size.x);
			}
			lost = false;

		});
		
		for (int i = 0; i < GateNumber; i++) {
			spawnNewGate(i * (size.x / GateNumber) + size.x);
		}
		
		lastScorePos = (int)pipes.get(0).getPos().x % (int)(size.x / GateNumber);
		
		NotGUI n = new NotGUI();		
		n.start();
		
		bScore = new Button();
		bMaxScore = new Button();
		
		bScore.setMaxWidth(size.x * 0.25);
		bScore.setMinWidth(size.x * 0.25);
		bScore.setPrefWidth(size.x * 0.25);
		
		bMaxScore.setMaxWidth(size.x * 0.25);
		bMaxScore.setMinWidth(size.x * 0.25);
		bMaxScore.setPrefWidth(size.x * 0.25);
		
		root.getChildren().add(bScore);
		root.getChildren().add(bMaxScore);
		
		bScore.setLayoutX(0);
		bScore.setLayoutY(0);
		
		bScore.setText("Score : " + score);
		bScore.setStyle(
                "-fx-base: #e5b46b; " +
                "-fx-font-size: 26px; " +
                "-fx-text-background-color: whitesmoke;"
        );
				
		bMaxScore.setText("Best Score : " + maxScore);
		bMaxScore.setStyle(
                "-fx-base: #ba71ce; " +
                "-fx-font-size: 26px; " +
                "-fx-text-background-color: whitesmoke;"
        );
		
		bMaxScore.setLayoutX(size.x  - size.x * 0.25);
		bMaxScore.setLayoutY(0);
		
		//Strange, change the sizes and pos of all nodes
		//stage.setResizable(false);
				
	}
	
	@Override
	public void stop() {
		running = false;
	}

	public static void main(String[] args) {
		launch();
	}
	
	class NotGUI extends Thread {
		
		private long l1 = System.currentTimeMillis();
		private long l2 = System.currentTimeMillis();
		
		@Override
		public void run() {

			while (running) {
				l1 = System.currentTimeMillis();

				if ((l1 - l2) >= 1000/90) {
					l2 = l1;
					update();
				}

			}
			
			System.out.println("Update thread stopped");
		}
	}
	
	class Pipe extends Button {
		
		private Vector2F pos;
		public static final float speed = -1.5f;
		private Label label;
		private boolean top;
		private Vector2F rSize;
		
		public Pipe (boolean top) {
			super();			
			
			this.top = top;
						
			this.setStyle("-fx-base: green; ");
			
			label = new Label("PIPE");
			label.setStyle("-fx-font-size: 20px; " +
					"-fx-text-background-color: whitesmoke;");
						
		}
		
		public void init(float pipe_width, float initPos) {
			label.setAlignment(Pos.CENTER);
			label.setRotate(-90);
			
			rSize = new Vector2F(size.x / 23, pipe_width);
			
			label.setPrefSize(pipe_width, size.x / 23);
			label.setMaxSize(pipe_width, size.x / 23);
			label.setMinSize(pipe_width, size.x / 23);
			
			if (top)
				pos = new Vector2F(initPos, 0);
			else
				pos = new Vector2F(initPos, (float)(size.y - label.getPrefWidth()));
			
			Platform.runLater(new Runnable() {
				
				@Override
				public void run() {
					Pipe.this.setGraphic(new Group(label));
					Pipe.this.setLayoutX(pos.x);
					Pipe.this.setLayoutY(pos.y);
				}
			});	
						
		}
		
		public void update() {
			if (pos.x < -rSize.x) {
				initPipe(this, size.x);
			}
			
			if (pos.x - size.x < 5 && pos.x - size.x > 0)
				pos.x = size.x;
			
			Platform.runLater(new Runnable() {
				
				@Override
				public void run() {
					Pipe.this.setLayoutX(pos.x);
				}
			});

			pos.x += speed;			
		}
		
		public Vector2F getPos() {
			return pos;
		}
		
		public Vector2F getRealSize() {
			return rSize;
		}
	}
	
	class Bird extends Button {
		private Vector2F pos;
		private float speed;
		private float acceleration;

		
		public Bird() {
			super();
			pos = new Vector2F(size.x / 4, size.y / 2);
			speed = 0.2f;
			acceleration = 0.08f;
			
			this.setLayoutX(pos.x);
			this.setLayoutY(pos.y);
			
			this.setText("BIRD");
			this.setStyle(
	                "-fx-base: orange; " +
                    "-fx-font-size: 20px; " +
                    "-fx-text-background-color: whitesmoke;"
	        );
		}
		
		public void update() {
			
			Platform.runLater(new Runnable() {
				
				@Override
				public void run() {
					Bird.this.setLayoutX(pos.x);
					Bird.this.setLayoutY(pos.y);
				}
			});
			
			speed += acceleration;
			pos.y += speed;
			
			if (pos.y >= size.y - this.getHeight())
				pos.y = (float)(size.y - this.getHeight());
			
			if (pos.y <= 0)
				pos.y = 0;
					
		}
		
		public void fly() {
			if (speed >= 0)
				speed = -4.5f;
		}
		
		
	}
	
	public void initPipe(Pipe p, float pos) {
		Random rd = new Random();
		float s = rd.nextFloat() * size.y * 0.15f + size.y * 0.25f;
		
		p.init((size.y - s)/2.f, pos);
	}
	
	public void spawnNewGate(float xpos) {
		Pipe temp1 = new Pipe(true);
		Pipe temp2 = new Pipe(false);
		
		initPipe(temp1, xpos);
		initPipe(temp2, xpos);
		
		root.getChildren().add(temp1);
		root.getChildren().add(temp2);
		
		pipes.add(temp1);
		pipes.add(temp2);
	}
	
	public boolean checkCollision(Bird b, Pipe p) {
		
		Vector2F bPos = new Vector2F((float)b.getLayoutX(), (float)b.getLayoutY());
		Vector2F bSize = new Vector2F((float)b.getWidth(), (float)b.getHeight());
		
		Vector2F pPos = p.getPos();
		Vector2F pSize = p.getRealSize();
		
		boolean lockx1 = false, lockx2 = false, locky1 = false, locky2 = false;
		
		if (bPos.x >= pPos.x && bPos.x <= pPos.x + pSize.x || bPos.x + bSize.x >= pPos.x && bPos.x + bSize.x <= pPos.x + pSize.x)
			lockx1 = true;
		
		if (bPos.y >= pPos.y && bPos.y <= pPos.y + pSize.y|| bPos.y + bSize.y >= pPos.y && bPos.y + bSize.y <= pPos.y + pSize.y )
			locky1 = true;
		
		if (pPos.x >= bPos.x && pPos.x <= bPos.x + pSize.x || pPos.x + pSize.x >= bPos.x && pPos.x + pSize.x <= bPos.x + bSize.x)
			lockx1 = true;
		
		if (pPos.y >= bPos.y && pPos.y <= bPos.y + bSize.y|| pPos.y + pSize.y >= bPos.y && pPos.y + pSize.y <= bPos.y + bSize.y )
			locky1 = true;
		
		return (lockx1 && locky1) || (lockx2 && locky2);
	}

}
